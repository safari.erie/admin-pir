@extends('layout.master')
@section('title','Daftar Perusahaan')
@section('content')
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Perusahaan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Perusahaan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                 <a href="{{url('perusahaan_add')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                  <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th style="width: 10px">No</th>
                        <th>NIB</th>
                        <th>Nama Perusahaan</th>
                        <th>No PMA</th>
                        <th>No KMK</th>
                        <th style="width: 40px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>9120104321696</td>
                            <td>
                                BATU MAS SEJAHTERA
                            </td>
                            <td>
                                PA62788
                            </td>
                            <td>
                                2/PABEAN-PB/PMA/2021
                            </td>
                            <td style="display:flex">
                                    <a href="" class="btn btn-info mr-2" data-tooltip="tooltip" data-placement="top" title="detail"> <i class="fa fa-bars"></i> </a>
                                    <a href="" class="btn btn-success mr-2" data-tooltip="tooltip" data-placement="top" title="Edit"> <i class="fa fa-edit"></i></a>
                                    <a href="" class="btn btn-danger mr-2" data-tooltip="tooltip" data-placement="top" title="hapus"> <i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->

      </div><!-- /.container-fluid -->
    </section>



    <!-- Modal -->
<div class="modal fade" id="modal-bentuk-daerah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
            Bentuk Daerah
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        <div class="form-group row">
            <label for="tingkat" class="col-sm-3 col-form-label"> Tingkat</label>
            <div class="col-sm-8">
            <input type="text" class="form-control" value="" placeholder="Input Level" id="tingkat">
            </div>
        </div>
        <div class="form-group row">
            <label for="bentukDaerah" class="col-sm-3 col-form-label">Bentuk Daerah</label>
            <div class="col-sm-9">
            <input type="text" class="form-control" id="bentukDaerah" placeholder="Input Bentuk Daerah">
            </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
    <script>
        function showModal(){
            $('#modal-bentuk-daerah').modal();
        }
    </script>

@endpush
