@extends('layout.master')
@section('title','Daftar Perusahaan')
@section('content')
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Perusahaan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Perusahaan Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Tambah Perusahaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="nib" class="col-sm-2 col-form-label">NIB</label>
                    <div class="col-sm-6">
                      <input type="number" class="form-control" id="nib" placeholder="Input NIB">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Nama Perusahaan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputNamaPerusahaan" placeholder="Input Nama Perusahaan">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPa" class="col-sm-2 col-form-label">No. PA</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPa" placeholder="Input Pa">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="jenisPerusahaan" class="col-sm-2 col-form-label">Jenis Perusahaan</label>
                    <div class="col-sm-10 mt-2">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                        <label class="form-check-label" for="inlineRadio1">PMA</label>
                        </div>
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">PMDN</label>
                        </div>
                    </div>
                  </div>

                    <div class="col-md-12 title-form">
                        Detail Perusahaan
                    </div>
                    <div class="form-row mt-4">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover nowrap myTableDetilPerusahaan" id="myTableDetilPerusahaans">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Judul</th>
                                        <th>Alamat</th>
                                        <th>NO Telepon</th>
                                        <th>PIC</th>
                                        <th>Provinsi</th>
                                        <th>Kabupaten</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <button type="button" onclick="addDetilPerusahaan();" class="btn btn-info btn-block mb-4"><i class="fa fa-plus"></i> Tambah </button>
                    </div>
                    <hr />
                    <div class="col-md-12 title-form">
                        Detail Klbi
                    </div>
                    <div class="form-row mt-4">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover nowrap myTableKlbi" id="myTableKlbi">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Judul KLBI</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <button type="button" onclick="addDetilKlbi()" class="btn btn-info btn-block mb-4"><i class="fa fa-plus"></i> Tambah </button>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Simpan</button>
                  <button type="submit" class="btn btn-default float-right">Batal</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->

      </div><!-- /.container-fluid -->
    </section>


@endsection

@push('scripts')
    <script>

        var counter = 2;
        var counterKlbi = 2;

        var counterDetilPerusahaan = 1;
        var counterDetilKlbi = 1;
        function addDetilPerusahaan(){
            let newRow = $("<tr>");
            let column = "";
            column += '<td> <label> '+`${counterDetilPerusahaan}`+'</label> </td>';
            column += '<td> <input type="text" class="form-control" id="Judul' + counterDetilPerusahaan+'" name="Judul"> </td>';
            column += '<td> <input type="text" class="form-control" id="Alamat' + counterDetilPerusahaan +'" name="Alamat"> </td>';
            column += '<td> <input type="text" class="form-control" id="NoTelepon' + counterDetilPerusahaan + '" name="No_tlp"> </td>';
            column += '<td> <input type="text" class="form-control" id="Pic' + counterDetilPerusahaan + '" name="Pic"> </td>';
            column += '<td> <select class="form-control select2 " name="Provs[]" id="Prov' + counterDetilPerusahaan + '" > <option> -- Pilih Prov --</option> <option> Aceh</option></select> </td>';
            column += '<td> <select class="form-control select2 " name="Kabs[]" id="Kab' + counterDetilPerusahaan + '" > <option> -- Pilih Kabupaten Kota --</option> <option> Bireun</option></select> </td>';
            column += '<td> <button type="button" class="btn  btn-danger removeDetilPerusahaan" > Hapus </button> </td>';
            /* setTimeout(() => {
                buildDropdownDiklat(function (callback) {
                    $('select.DiklatId').html(callback);
                });

            }, 1000) */
            newRow.append(column);
            $(".myTableDetilPerusahaan").append(newRow);
            counterDetilPerusahaan++;

        }

        function addDetilKlbi(){
            let newRow = $("<tr>");
            let column = "";
            column += '<td> <label> '+`${counterDetilKlbi}`+'</label> </td>';
            column += '<td> <select class="form-control select2 " name="kodes[]" id="kode' + counterDetilKlbi + '" > <option> -- Pilih Kode --</option> <option> 123</option></select> </td>';
            column += '<td> <select class="form-control select2 " name="juduls[]" id="judul' + counterDetilKlbi + '" > <option> -- Pilih Judul --</option> <option> ABC</option></select> </td>';
            column += '<td> <button type="button" class="btn  btn-danger removeDetilKlbi" > Hapus </button> </td>';

            newRow.append(column);
            $(".myTableKlbi").append(newRow);
            counterDetilKlbi++;

        }

        $("table.myTableDetilPerusahaan").on("click", ".removeDetilPerusahaan", function (event) {
            console.log('tes')
            $(this).closest("tr").remove();


            counterDetilPerusahaan -= 1
            res_count = 0;
            if (counter == 2) {
                res_count = 1
            } else {
                res_count = counter;
            }
        });

        $("table.myTableKlbi").on("click", ".removeDetilKlbi", function (event) {
            console.log('tes')
            $(this).closest("tr").remove();


            counterDetilKlbi -= 1
            res_count = 0;
            if (counterKlbi == 2) {
                res_count = 1
            } else {
                res_count = counterKlbi;
            }
        });


    </script>

@endpush
