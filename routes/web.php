<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboards.index');
});

Route::get('/bentuk_daerah',function(){
    return view('daerahs.bentuk_daerah');
});

Route::get('/perusahaan',function(){
    return view('umkms.perusahaan_list');
});

Route::get('/perusahaan_add',function(){
    return view('umkms.perusahaan_add');
});
